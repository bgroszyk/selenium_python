import time
import pytest

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

from pages.arena.home_page import HomePage
from pages.arena.login_page import LoginPage
from pages.arena.project_page import ProjectPage

administrator_email = 'administrator@testarena.pl'

@pytest.fixture
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    login_page = LoginPage(browser)
    login_page.visit()
    login_page.login(administrator_email, 'sumXQQ72$L')

    yield browser
    browser.quit()


def test_testarena_login(browser):
    user_email = browser.find_element(By.CSS_SELECTOR, '.user-info small').text
    assert user_email == administrator_email

    home_page = HomePage(browser)
    home_page.click_on_administrator_link()

    project_page = ProjectPage(browser)
    project_page.click_on_add_project_link()
    generated_project_name = project_page.inject_data_to_new_project_form()
    project_page.save_new_project_form()
    project_page.click_on_project_list_link()
    project_page.search_for_project(generated_project_name)

    time.sleep(10)
