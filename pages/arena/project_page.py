import random
import string
import json

from selenium.webdriver.common.by import By


def get_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))


class ProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def search_for_project(self, search_term):
        self.browser.find_element(By.CSS_SELECTOR, '#search').send_keys(search_term)
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    def verify_projects_found(self, search_term):
        found_projects = self.browser.find_elements(By.CSS_SELECTOR, 'tbody tr')
        assert len(found_projects) > 0

        names = self.browser.find_elements(By.CSS_SELECTOR, 'tbody tr td:nth-of-type(1)')
        for name in names:
            assert search_term in name.text.lower()

    def click_on_add_project_link(self):
        self.browser.find_element(By.CSS_SELECTOR, 'a.button_link:nth-of-type(1)').click()

    def click_on_project_list_link(self):
        self.browser.find_element(By.CSS_SELECTOR, '.menu .item2 a').click()

    # Injects data to new project form and returns project name.
    def inject_data_to_new_project_form(self):
        new_project_name = get_random_string(random.randint(5, 10))
        project_name_input = self.browser.find_element(By.CSS_SELECTOR, '#name')

        new_project_prefix = get_random_string(random.randint(5, 10))
        project_prefix_input = self.browser.find_element(By.CSS_SELECTOR, '#prefix')

        project_name_input.send_keys(new_project_name)
        project_prefix_input.send_keys(new_project_prefix)

        return str(new_project_name)

    def save_new_project_form(self):
        project_save_button = self.browser.find_element(By.CSS_SELECTOR, '#save')
        project_save_button.click()
